#!/usr/bin/env bash
set -e
echo "${EX_BOOK_VAULT_PASS}" > vaultpass
ansible-playbook --vault-id vaultpass -i 'localhost,' --connection=local --extra-vars "helm_image_tag_test=latest helm_image_tag_prod=latest" gitlab-playbook.yml
