FROM alpine:latest

RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY ex-book-admin-api-go /bin/ex-book-admin-api-go

EXPOSE 8080

ENTRYPOINT ["/bin/ex-book-admin-api-go"]

CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-m"]
