[IMPORTANT]
====
This environment is most useful for checking if containers created with a use of `single-stage.Dockerfile` and
`multi-stage.Dockerfile` are valid and for end-to-end testing of prepared service.
====

In this project we have two versions of a Dockerfile. Each for a different use case.

First one `single-stage.Dockerfile` is used by our Gitlab CI as we have two separate jobs in it, and we need speed here.
Second one `multi-stage.Dockerfile` is used by end-to-end tests which we will be running locally.

Let's look at the `single-stage.Dockerfile` first:

[source,dockerfile]
----
include::../single-stage.Dockerfile[]
----

as you can see this file expects that our application will be build and that this build was done with a Linux system.

****
If you are using macOS you can use special script `build-linux.sh` which looks like this:

[source,shell]
----
include::../build-linux.sh[]
----

and will build binary for this docker file.
****

Now let's look at the `multi-stage.Dockerfile`:

[source,dockerfile]
----
include::../multi-stage.Dockerfile[]
----

only difference is that this version of a Dockerfile will build binary in the first stage and then use it in the second stage.

****
Always check if you can generate docker containers using both files if even in only one of it there will be a change. You can use prepared scripts: `build-single-stage-container.sh` and `build-multi-stage-container.sh`.

This will minify number of failed pipelines.
****

With help of `docker-compose-test.yml` file we can create environment which can be used for end-to-end testing.

This file looks like this:

[source,yaml]
----
include::../docker-compose-test.yml[]
----

as you can see there is `OPENAPI` section which is starting swagger UI and Editor and will use `public/v1/openapi.json` for the UI.

You can start this environment by just running:

[source,bash]
----
docker-compose up
----

each time you will change anything in your application please remember to rebuild it with:

[source,bash]
----
docker-compose build
----

