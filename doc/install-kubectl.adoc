Command line tool kubectlfootnote:[Command line tool (kubectl) https://kubernetes.io/docs/reference/kubectl/] is commonly used to manage K8sfootnote:[Kubernetes https://kubernetes.io/] and K3sfootnote:[Lightweight Kubernetes https://k3s.io/] clusters and we will need it for our daily work.

There is many methods of installing it and the most common one is available in this link: https://kubernetes.io/docs/tasks/tools/

Other method of installing it and prefered by Me is brew.

[source,bash]
----
brew install kubectl
----
