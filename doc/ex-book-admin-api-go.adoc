= Book Admin API
Arkadiusz Tułodziecki <atulodzi@gmail.com>
1.0, Aug 31, 2022: AsciiDoc article template
:toc:
:source-highlighter: rouge
:encoding: utf-8
:icons: font
:url-quickref: https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/

ifdef::backend-html5[]
PDF version of this article:
link:ex-book-admin-api-go.pdf/[ex-book-admin-api-go.pdf]
endif::[]

== Local Development Environment

This documentation shows how to create local development environment on Linux Desktop or macOS.

This projects needs to be cloned into `~/go/src/gitlab.com/ldath-core/examples` folder in case
when `$GOPATH` is set to `~/go`.

=== Tools

Those tools need to be installed prior to configuring a project to work.

==== Brew - optional

****
We can use different ways of installing other tools but this one simplify process a lot.
****

include::install-brew.adoc[]

==== Docker Engine - required

include::install-docker-engine.adoc[]

==== Docker Compose - required

include::install-docker-compose.adoc[]

==== Ansible - required

include::install-ansible.adoc[]

==== K3d - optional

[WARNING]
This tool is required when we need to work with the deployment charts and test them

include::install-k3d.adoc[]

==== Helm - optional

[WARNING]
This tool is required when we need to work with the deployment charts and test them

include::install-helm.adoc[]

We will be using this tool to check prepared for our microservice helm chart - which will be used to deploy our service to the production environment. We will be testing it by using our local K3s cluster.

==== Trivy - optional

[WARNING]
This tool is required when we are checking our docker image and application security

include::install-trivy.adoc[]

==== Grype - optional

[WARNING]
This tool is required when we are checking our docker image and application security

include::install-grype.adoc[]

=== Configuring project

include::configuring.adoc[]

=== Development

==== Service Development and Continuous Integration

include::development-service.adoc[]

==== Continuous Delivery and end-to-end testing

include::development-dockerfile.adoc[]

==== Continuous Deployment Testing

include::development-kubernetes.adoc[]

image:https://i.creativecommons.org/l/by/4.0/88x31.png[Creative Commons Licence,88,31] This work is licensed under a http://creativecommons.org/licenses/by/4.0/[Creative Commons Attribution 4.0 International License]
