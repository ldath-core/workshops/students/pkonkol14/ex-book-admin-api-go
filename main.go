/*
Copyright © 2022 Artkadiusz Tułodziecki <atulodzi@gmail.com>

*/
package main

import "gitlab.com/ldath-core/examples/ex-book-admin-api-go/cmd"

func main() {
	cmd.Execute()
}
