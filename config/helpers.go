package config

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"testing"
)

func getTestConfig() (*Config, error) {
	cfgFile := os.Getenv("CFG_FILE")
	viper.SetConfigFile(cfgFile)
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return LoadConfig("text")
}

func GetTestSetup(t *testing.T, ctx context.Context) (l *logrus.Logger, d *mongo.Database, c *Config) {
	// Logger
	l = logrus.New()

	// Config
	c, e := getTestConfig()
	if e != nil {
		t.Fatalf("Error: %s", e.Error())
	}

	// MongoDB
	mongoDbUri := PrepareMongoDbUri(c)
	client, e := mongo.NewClient(options.Client().ApplyURI(mongoDbUri))
	if e != nil {
		t.Fatalf("Failed preparing client for the database: %s", mongoDbUri)
	}

	e = client.Connect(ctx)
	if e != nil {
		t.Fatalf("Failed connecting to the database: %s", mongoDbUri)
	}

	d = client.Database(c.MongoDb.Database)

	return l, d, c
}
