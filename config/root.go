package config

import (
	"errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/url"
	"os"
	"strings"
	"time"
)

type LoggerConfig struct {
	Level string `mapstructure:"level"`
}

type MongoDbConfig struct {
	Srv                    bool   `mapstructure:"srv"`
	Host                   string `mapstructure:"host"`
	AuthenticationDatabase string `mapstructure:"authentication-database"`
	User                   string `mapstructure:"user"`
	Password               string `mapstructure:"password"`
	Database               string `mapstructure:"database"`
	ReplicaSet             string `mapstructure:"replica-set"`
}

type ServerConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}

type Config struct {
	Environment string        `mapstructure:"env"`
	Server      ServerConfig  `mapstructure:"server"`
	Logger      LoggerConfig  `mapstructure:"logger"`
	MongoDb     MongoDbConfig `mapstructure:"mongodb"`
}

func LoadConfig(format string) (*Config, error) {
	var config *Config

	logrus.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
		//logrus.SetReportCaller(true)
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	if err := viper.Unmarshal(&config); err != nil {
		logrus.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Errorln("Broken config file")
		return config, err
	}
	if config.Logger.Level == "" {
		logrus.Errorln("Missing config file")
		time.Sleep(5 * time.Minute)
		return config, errors.New("missing config file")
	}
	return config, nil
}

func PrepareMongoDbUri(c *Config) string {
	var mongoScheme string

	if c.MongoDb.Srv {
		mongoScheme = "mongodb+srv"
	} else {
		mongoScheme = "mongodb"
	}
	uri := url.URL{
		Scheme: mongoScheme,
		User:   url.UserPassword(c.MongoDb.User, c.MongoDb.Password),
		Host:   c.MongoDb.Host,
		Path:   "/",
	}
	q := uri.Query()
	if c.MongoDb.AuthenticationDatabase != "" {
		q.Set("authSource", c.MongoDb.AuthenticationDatabase)
	}
	if c.MongoDb.ReplicaSet != "" {
		q.Set("replicaSet", c.MongoDb.ReplicaSet)
	}
	uri.RawQuery = q.Encode()
	return uri.String()
}
