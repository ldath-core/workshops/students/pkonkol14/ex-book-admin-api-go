package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go/app"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go/config"
)

var port string  //set port
var bind string  //set bind
var migrate bool //set migrate
var load bool    //set load
var cors bool    //set cors

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		logFormat := "json"
		config, err := config.LoadConfig(logFormat)
		if err == nil {
			config.Server.Host = bind
			config.Server.Port = port
			app.ConfigAndRunApp(config, logFormat, migrate, load, cors)
		}
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serveCmd.PersistentFlags().String("foo", "", "A help for foo")
	serveCmd.PersistentFlags().StringVarP(&bind, "bind", "b", "127.0.0.1", "This flag sets the IP to bind for our API server - overwrites config")
	serveCmd.PersistentFlags().StringVarP(&port, "port", "p", "8080", "This flag sets the port of our API server - overwrites config")
	serveCmd.PersistentFlags().BoolVarP(&migrate, "migrate", "m", false, "This decide if we should migrate or not when starting")
	serveCmd.PersistentFlags().BoolVarP(&load, "load", "l", false, "This decide if we should load test data or not when starting")
	serveCmd.PersistentFlags().BoolVarP(&cors, "cors", "c", false, "This decide if we should enable cors - in the Prod NGiNX is solving cors for us")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
