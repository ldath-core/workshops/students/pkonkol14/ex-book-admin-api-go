package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go/app/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func CreateTestData(ctx context.Context, database *mongo.Database, logger *logrus.Logger) error {
	admin, err := model.NewAdmin("atulodzi@gmail.com",
		"Main",
		"Administrator",
		"NicePass4Me")
	if err != nil {
		return err
	}

	insertTestAdmin(ctx,
		database,
		logger,
		admin)
	logger.Info("Loading test data completed")
	return nil
}

func insertTestAdmin(ctx context.Context, database *mongo.Database, logger *logrus.Logger, admin *model.Admin) {
	result, err := database.Collection("admin").InsertOne(ctx, admin)
	if err != nil {
		switch err.(type) {
		case mongo.WriteException:
			logger.Warningf("email: %s already exists in database.", admin.Email)
		default:
			logger.Errorln(err)
		}
		return
	}
	admin.ID = result.InsertedID.(primitive.ObjectID)
	logger.Debugf("admin: %s inserted with ID: %s", admin.Email, admin.ID.Hex())
}
