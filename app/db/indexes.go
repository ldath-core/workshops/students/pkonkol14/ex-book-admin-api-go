package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"time"
)

// CreateUniqueIndex will create mongo index for collection and keys that sent.
func CreateUniqueIndex(ctx context.Context, collection *mongo.Collection, keys bsonx.Doc, name string) (string, error) {
	index := mongo.IndexModel{}
	index.Keys = keys
	unique := true
	index.Options = &options.IndexOptions{
		Name:   &name,
		Unique: &unique,
	}
	opts := options.CreateIndexes().SetMaxTime(10 * time.Second)
	return collection.Indexes().CreateOne(ctx, index, opts)
}
