package app

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go/app/db"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go/app/handler"
	"gitlab.com/ldath-core/examples/ex-book-admin-api-go/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

// App has the mongo database and router instances
type App struct {
	Router *mux.Router
	DB     *mongo.Database
	RDB    *redis.Client
	Logger *logrus.Logger
	Config *config.Config
}

// ConfigAndRunApp will create and initialize App structure. App factory functions.
func ConfigAndRunApp(config *config.Config, logFormat string, migrate, load bool, corsEnabled bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat)

	if load {
		// Drop all prior to create test data
		err := db.DropAllCollections(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}

		// Create Test DATA
		err = db.CreateTestData(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	if migrate {
		// Migrate Database to the latest version
		err := db.MigrateDatabase(ctx, app.DB, app.Logger)
		if err != nil {
			app.Logger.Fatal(err)
		}
	}

	app.Run(fmt.Sprintf("%s:%s", config.Server.Host, config.Server.Port), corsEnabled)
}

// ConfigAndLoadTestData will configure and initialize core App structure and then only Load Test data.
func ConfigAndLoadTestData(config *config.Config, logFormat string) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat)
	defer func() {
		if err := app.DB.Client().Disconnect(ctx); err != nil {
			app.Logger.Fatal(err)
		}
	}()

	// Drop all prior to create test data
	err := db.DropAllCollections(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}

	// Migrate Database to the latest version
	err = db.MigrateDatabase(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}

	// Create Test DATA
	err = db.CreateTestData(ctx, app.DB, app.Logger)
	if err != nil {
		app.Logger.Fatal(err)
	}
}

func ConfigAndMigrateDb(config *config.Config, logFormat string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	app := new(App)
	app.Initialize(ctx, config, logFormat)
	defer func() {
		if err := app.DB.Client().Disconnect(ctx); err != nil {
			app.Logger.Error(err)
		}
	}()

	// Migrate Database to the latest version
	return db.MigrateDatabase(ctx, app.DB, app.Logger)
}

// Initialize the app with config
func (app *App) Initialize(ctx context.Context, config *config.Config, logFormat string) {
	// Configuration
	app.Config = config

	// Configure Logger
	app.setLogger(config, logFormat)

	// Configure MongoDB
	app.setMongoDb(ctx, config)

	// Router
	app.Router = mux.NewRouter()
	app.UseMiddleware(handler.JSONContentTypeMiddleware)
	app.setRouters()
}

func (app *App) setLogger(config *config.Config, format string) {
	app.Logger = logrus.New()
	app.Logger.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		app.Logger.SetFormatter(&logrus.JSONFormatter{})
		//app.Logger.SetReportCaller(true)
	} else {
		app.Logger.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	lvl, err := logrus.ParseLevel(config.Logger.Level)
	if err != nil {
		lvl = logrus.WarnLevel
		app.Logger.SetLevel(lvl)
		app.Logger.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Warning("Configuration Error")
	} else {
		app.Logger.SetLevel(lvl)
	}
}

func (app *App) setMongoDb(ctx context.Context, c *config.Config) {
	mongoDbUri := config.PrepareMongoDbUri(c)

	// Connect to my cluster
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoDbUri))
	if err != nil {
		app.Logger.Fatal(err)
	}

	app.DB = client.Database(c.MongoDb.Database)
}

// UseMiddleware will add global middleware in router
func (app *App) UseMiddleware(middleware mux.MiddlewareFunc) {
	app.Router.Use(middleware)
}

func (app *App) setRouters() {
	c := handler.Controller{
		MDB:    app.DB,
		RDB:    app.RDB,
		Logger: app.Logger,
		Config: app.Config,
	}
	app.Get("/v1/health", c.GetHealth)
	app.Post("/v1/admins", c.CreateAdmin)
	app.Patch("/v1/admins/{id}", c.UpdateAdmin)
	app.Put("/v1/admins/{id}", c.UpdateAdmin)
	app.Get("/v1/admins/{id}", c.GetAdmin)
	app.Delete("/v1/admins/{id}", c.DeleteAdmin)
	app.Get("/v1/admins", c.GetAdmins)
	app.Get("/v1/admins", c.GetAdmins, "page", "{page}")
	app.Router.NotFoundHandler = http.HandlerFunc(c.NotFound)
}

// Get will register Get method for an endpoint
func (app *App) Get(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("GET").Queries(queries...)
}

// Post will register Post method for an endpoint
func (app *App) Post(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("POST").Queries(queries...)
}

// Put will register Put method for an endpoint
func (app *App) Put(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("PUT").Queries(queries...)
}

// Patch will register Patch method for an endpoint
func (app *App) Patch(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("PATCH").Queries(queries...)
}

// Delete will register Delete method for an endpoint
func (app *App) Delete(path string, endpoint http.HandlerFunc, queries ...string) {
	app.Router.HandleFunc(path, endpoint).Methods("DELETE").Queries(queries...)
}

// Run will start the http server on host that you pass in. host:<ip:port>
func (app *App) Run(host string, corsEnabled bool) {
	// use signals for shutdown server gracefully.
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	go func() {
		if corsEnabled {
			appHandler := cors.New(cors.Options{
				AllowedOrigins:   []string{"*"},
				AllowedHeaders:   []string{"*"},
				AllowCredentials: true,
				Debug:            false,
			}).Handler(app.Router)
			app.Logger.Fatal(http.ListenAndServe(host, appHandler))
		} else {
			app.Logger.Fatal(http.ListenAndServe(host, app.Router))
		}
	}()
	app.Logger.Infof("Server is listning on %s://%s", "http", host)

	sig := <-sigs
	app.Logger.Infoln("Signal: ", sig)

	app.Logger.Infoln("Stopping MongoDB Connection...")
	err := app.DB.Client().Disconnect(context.Background())
	if err != nil {
		app.Logger.Infoln(err)
	}
}
